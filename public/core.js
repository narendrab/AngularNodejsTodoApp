// // public/core.js
// var scotchTodo = angular.module('scotchTodo', []);

// function mainController($scope, $http) {
//     $scope.formData = {};

//     // when landing on the page, get all todos and show them
//     $http.get('/api/todos')
//         .success(function(data) {
//             $scope.todos = data;
//             console.log(data);
//         })
//         .error(function(data) {
//             console.log('Error: ' + data);
//         });

//     // when submitting the add form, send the text to the node API
//     $scope.createTodo = function() {
//         $http.post('/api/todos', $scope.formData)
//             .success(function(data) {
//                 $scope.formData = {}; // clear the form so our user is ready to enter another
//                 $scope.todos = data;
//                 console.log(data);
//             })
//             .error(function(data) {
//                 console.log('Error: ' + data);
//             });
//     };

//     // delete a todo after checking it
//     $scope.deleteTodo = function(id) {
//         $http.delete('/api/todos/' + id)
//             .success(function(data) {
//                 $scope.todos = data;
//                 console.log(data);
//             })
//             .error(function(data) {
//                 console.log('Error: ' + data);
//             });
//     };

// }
// 
var Todomodule = angular.module('Todomodule', []);

function mainController($scope, $http) {
    $scope.formData = {};
    $http.get('/api/todos').success(function(data) {
        console.log("result:", data);
        $scope.todos = data;
    }).error(function(data) {
        console.log("Error:", data);
    });

    $scope.deleteTodo = function(id) {
        $http.post('/api/todo/delete/' + id).success(function(data) {
            $scope.todos = data;
            console.log("in data")
        }).error(function(data) {
            console.log("Error occured during the delete the todo")
        });
    }

    $scope.createTodo = function() {
        // $scope.submitForm = function() {
        console.log("$scope.todoform", $scope.todoform);
        console.log("todoform", todoform);

        if ($scope.todoform.$valid) {
            $http.post('/api/add/todo', $scope.formData).success(function(data) {
                $scope.formdata = {};
               // viewModel.data = {};
                // $scope.todoform.$setUntouched();
                // $scope.todoform.$setPristine();
                $scope.todos = data;
            }).error(function(data) {
                console.log("Error occured while getting adding the todo");
            })
        } else {
            console.log("$scope.todoform", $scope.todoform);

            console.log("validation is failed")
        }
        // }

    }

    $scope.reset = function() {
        $scope.todoform.name = null;
    }

}
