// server.js

// set up ========================
var express = require('express');
var app = express(); // create our app w/ express
var mongoose = require('mongoose'); // mongoose for mongodb
var morgan = require('morgan'); // log requests to the console (express4)
var bodyParser = require('body-parser'); // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
passport = require('passport');
FacebookStrategy = require('passport-facebook')
// configuration =================
// mongoose.connect('mongodb://node:nodeuser@mongo.onmodulus.net:27017/uwO3mypu');     // connect to mongoDB database on modulus.io

mongoose.connect('mongodb://localhost/test');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    console.log("Connected to mongo db");
});

//Creating a model
var Todo = mongoose.model('Todo', {
    text: String
});

app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.urlencoded({ 'extended': 'true' })); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");

//Now we are going to implement a route to get a all todos
app.post('/todo/get', function(req, res) {
    Todo.create({
        text: "eating",
        done: false
    }, function(err, result) {
        if (err) {
            console.log("error")
        } else {
            console.log("result")
            Todo.find(function(err, todos) {
                if (err)
                    res.send(err)
                res.json(todos);
            });
        }
    });
});

//Delete a todo based on given id
app.post('/api/todo/delete/:id', function(req, res) {
    console.log("in deleting the todo")
    var id = req.params.id;
    console.log("id", id);
    Todo.remove({ _id: id }, function(err, todo) {
        if (err) {
            res.send(err);
        } else {
            Todo.find(function(err, result) {
                if (err) {
                    res.send(err);

                } else {
                    res.json(result);
                }
            })
        }
    })
})
//getting a todo based on given id

app.post('/api/add/todo', function(req, res) {
    console.log("in deleting the todo")
    var text = req.body.text;
    console.log("req.body", req.body);
    Todo.create({ text: text }, function(err, result) {
        if (err) {
            res.send(err);
        } else {
            Todo.find(function(err, result) {
                if (err) {
                    res.send(err);

                } else {
                    res.json(result);

                }
            })
        }
    })
})

//get all todos
app.get('/api/todos', function(req, res) {
    Todo.find(function(err, result) {
        if (err) {
            res.send("Error occured")
        } else {
            res.json(result);
        }
    })
})
auth = require('./routes/auth');
app.use('/auth', auth);
app.use(passport.initialize());
app.use(passport.session());
passport.use(new FacebookStrategy({
    clientID: "1323302584417525",
    clientSecret:"79fab10ee1e8c79f9c3444cbbc9f4d28",
   // callbackURL: 'http://localhost:8080/auth/facebook/callback',
    profileFields: ['id', 'email', 'bio', 'picture.type(large)', 'name', 'about', 'location', 'hometown', 'birthday'],
    scope: ['publish_page', 'user_about_me']
  },
  function(accessToken, refreshToken, profile, cb) {
    // In this example, the user's Facebook profile is supplied as the user
    // record.  In a production-quality application, the Facebook profile should
    // be associated with a user record in the application's database, which
    // allows for account linking and authentication with other identity
    // providers.
    profile.access_token = accessToken;
    return cb(null, profile);
  }));
// passport.use(new FacebookStrategy({
//         clientID: 251456395313708,
//         clientSecret: "d7b4db3b971be38508f05c802ec293c1",
//         callbackURL: "http://localhost:3000/services/auth/callback",
//         profileFields: ['id', 'email', 'link', 'locale', 'name', 'timezone', 'updated_time', 'verified', 'displayName', 'picture.type(large)'],
//         scope: ['email']
//     },
//     function(accessToken, refreshToken, profile, done) {
//         process.nextTick(function() {
//             done(null, profile);
//         });
//     }));
passport.serializeUser(function(user, cb) {
  cb(null, user);
});

passport.deserializeUser(function(obj, cb) {
  cb(null, obj);
});
app.get('*', function(req, res) {
    res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});




// passport.use(new FacebookStrategy({
//     clientID: FACEBOOK_APP_ID,
//     clientSecret: FACEBOOK_APP_SECRET,
//     callbackURL: "http://localhost:8080//auth/facebook/callback"
//   },
//   function(accessToken, refreshToken, profile, done) {
//     User.findOrCreate(..., function(err, user) {
//       if (err) { return done(err); }
//       done(null, user);
//     });
//   }
// ));
